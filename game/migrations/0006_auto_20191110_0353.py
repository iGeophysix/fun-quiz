# Generated by Django 2.2.7 on 2019-11-10 03:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0005_game_created_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gamestage',
            name='question',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='game.Question'),
        ),
        migrations.CreateModel(
            name='GameStageCategories',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='game.QuestionCategory')),
                ('game_stage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='game.GameStage')),
            ],
        ),
    ]
