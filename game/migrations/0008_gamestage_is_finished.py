# Generated by Django 2.2.7 on 2019-11-10 04:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0007_auto_20191110_0426'),
    ]

    operations = [
        migrations.AddField(
            model_name='gamestage',
            name='is_finished',
            field=models.BooleanField(default=True),
            preserve_default=False,
        ),
    ]
