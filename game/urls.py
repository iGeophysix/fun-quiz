from django.urls import path
from game import views

app_name='game'

urlpatterns = [
    # SERVER SIDE
    path('newgame', views.new_game, name='new_game'),
    path('newstage', views.start_new_game_stage, name='start_new_game_stage'),
    # CLIENT SIDE
    path('', views.index, name='index'),
    path('lobby', views.lobby, name='lobby'),
    path('listplayersingame', views.list_players_in_game, name='list_players_in_game'),
    path('allready', views.all_ready, name='all_ready'),
    path('isgamestarted', views.is_game_started, name='is_game_started'),
    path('cat', views.category_selection, name='category_selection'),
    path('q/<category_id>', views.question, name='question'),
]
