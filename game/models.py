from django.db import models
from random import randint, shuffle


class QuestionCategory(models.Model):
    name = models.CharField(max_length=256)

    @classmethod
    def get_random_categories(cls):
        ids = list(cls.objects.all().values_list('id', flat=True))
        shuffle(ids)
        ids = ids[0:4]
        categories = list(cls.objects.filter(id__in=ids).values('id', 'name'))
        shuffle(categories)
        return categories


class Question(models.Model):
    text = models.CharField(max_length=1024)
    category = models.ForeignKey(QuestionCategory, on_delete=models.CASCADE)

    @classmethod
    def get_random_question(cls, category_id):
        question_ids_in_cat = tuple(cls.objects.filter(category_id=category_id).values_list('id', flat=True))
        random_id = question_ids_in_cat[randint(0, len(question_ids_in_cat)-1)]
        output = {'id': '', 'category': '', 'text': '', 'answers': []}
        question = cls.objects.get(id=random_id)
        output['id'] = question.id
        output['category'] = question.category.name
        output['text'] = question.text
        answers = list(Answer.objects.filter(question_id=random_id).values_list('text', flat=True))
        shuffle(answers)
        output['answers'] = answers
        return output


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.CharField(max_length=1024)
    is_correct = models.BooleanField()


class Player(models.Model):
    name = models.CharField(max_length=32)


class Game(models.Model):
    code = models.CharField(max_length=16)
    is_started = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now=True)


class PlayersInGame(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)


class GameStage(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    start_time = models.DateTimeField(auto_now=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, blank=True, null=True)
    is_finished = models.BooleanField()
    
    @staticmethod
    def new(game):
        stage = GameStage.objects.create(game=game, is_finished=False)
        random_categories = QuestionCategory.get_random_categories()
        categories = []
        for random_category in random_categories:
            categories.append(GameStageCategories.objects.create(game_stage=stage, category_id=random_category['id']).category)
        return stage, categories
    
    def set_question(self, category:QuestionCategory):
        self.question = Question.get_random_question(category_id=category.id)
        self.save()
        return self.question

class GameStageCategories(models.Model):
    game_stage = models.ForeignKey(GameStage, on_delete=models.CASCADE)
    category = models.ForeignKey(QuestionCategory, on_delete=models.CASCADE)
    

class GameStageCategoryVoting(models.Model):
    game_stage = models.ForeignKey(GameStage, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    category = models.ForeignKey(QuestionCategory, on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now=True)


class GameStageAnswers(models.Model):
    game_stage = models.ForeignKey(GameStage, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    time_taken = models.DecimalField(max_digits=8, decimal_places=4)
    time = models.DateTimeField(auto_now=True)
