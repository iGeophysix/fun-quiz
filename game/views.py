from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from .models import *
from random import randint, shuffle, choices
import string


# SERVER SIDE


def new_game(request):
    N = 6
    game_code = ''.join(choices(string.ascii_uppercase + string.digits, k=N))
    game = Game.objects.create(code=game_code)
    context = {'game_code': game_code}
    response = render(request, 'game/new_game.html', context=context)
    response.set_cookie('game_code', value=game_code)
    return response


def start_new_game_stage(request):
    game_code = request.GET['gameCode']
    game = Game.objects.get(code=game_code)
    stage, categories = GameStage.new(game=game)
    context = {
        "game_code": game_code,
        "stage_id": stage.id,
        "categories": categories
    }
    return render(request, 'game/server_category_selection.html', context)


# CLIENT SIDE


def index(request):
    context = {}
    return render(request, 'game/index.html', context=context)


def lobby(request):
    # check game exists
    try:
        game_code = request.GET['gameCode']
        game = Game.objects.get(code=game_code)
    except KeyError:
        return HttpResponse("Неправильный код игры")
    # get or create player
    player, created = Player.objects.get_or_create(name=request.GET['inputName'])
    # check user is in the game
    try:
        player_in_game = PlayersInGame.objects.get(game=game, player=player)
    except PlayersInGame.DoesNotExist:
        # check user is not in the game yet and don't let him in if the game is started
        if not game.is_started:
            player_in_game = PlayersInGame.objects.create(game=game, player=player)
        else:
            return HttpResponse("Простите, но игра уже началась...")

    context = {'player': player, 'game_code': game_code}
    response = render(request, 'game/lobby.html', context=context)
    response.set_cookie('player_id', value=player.id)
    response.set_cookie('game_code', value=game_code)
    return response


def list_players_in_game(request):
    try:
        game_code = request.COOKIES['game_code']
        players_in_game = list(PlayersInGame.objects.filter(game__code=game_code).values_list('player__name', flat=True))
        return JsonResponse({'status': 'ok', 'players': players_in_game})
    except Exception as exc:
        return JsonResponse({'status': 'error', 'error': str(exc)})


def all_ready(request):
    try:
        game_code = request.COOKIES['game_code']
        game = Game.objects.get(code=game_code)
        game.is_started = True
        game.save()
        return JsonResponse({'status': 'ok'})
    except Exception as exc:
        return JsonResponse({'status': 'err', 'error': str(exc)})


def is_game_started(request):
    try:
        game_code = request.COOKIES['game_code']
        game = Game.objects.get(code=game_code)
        return JsonResponse({'status': 'ok', 'is_started': game.is_started})
    except Exception as exc:
        return JsonResponse({'status': 'err', 'error': str(exc)})


def category_selection(request):
    try:
        game_code = request.COOKIES['game_code']
        stage = GameStage.objects.get(game__code=game_code, is_finished=False)
        categories = list(GameStageCategories.objects.filter(game_stage=stage).values_list('category__name', flat=True))
        context = {'status':'ok', 'ready': True, 'categories': categories}
    except GameStage.DoesNotExist:
        context = {'status': 'ok', 'ready': False }
    except Exception as exc:
        context = {'status': 'err', 'error': str(exc)}
    
    if request.is_ajax():
        return JsonResponse(context)
    else:
        return render(request, 'game/category_selection.html', context=context)


def question(request, category_id):
    q = Question.get_random_question(category_id=category_id)
    context = {'question': q}
    return render(request, 'game/question.html', context=context)
